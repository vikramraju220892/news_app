import React,{Component,useReducer} from 'react';
import {Text,FlatList, Image,View,TouchableWithoutFeedback, Button,ActivityIndicator} from 'react-native'
import { createStore } from 'redux'
import{newsStore} from './Stores'

// const store = createStore(newsStore, [{Id:1,title: "Retail Bitcoin Traders Repeatedly Shaken Out While Following Trends",
// description: "Retail buyers helped push the price of Bitcoin back up toward $10,000 after the Black Thursday crash to below $4,000. Low prices proved to be too attractive to pass up, and traders went against the trend. But following the initial storm of buying, retail trad…",urlToImage: "https://bitcoinist.com/wp-content/uploads/2020/06/bitcoin-binance-retail-traders-shutterstock_517613890-1920x1280.jpg"},
// {Id:2, title: "Scammer Spoofs SMS Identifier to Steal Funds From Bitcoin User",
// description: "A Bitcoin exchange transaction resulted in a user getting scammed as a spoofed SMS message from a payment app made it seem that the attacker executed the payment.",urlToImage: "https://s3.cointelegraph.com/storage/uploads/view/db735779792bc5b20977ead45e7bb56b.jpg"},{Id:3, title: "Chain Reaction - Avoiding Pitfalls & Crisis Investing",
// description: "Kevin Kelly welcomes Cullen Roche, Founder & CIO of Orcam Financial Group, for a lively discussion on crisis investing, elevated risks in fixed income & Treasuries, the growing disconnect between stocks and economic readings, and much more!Visit Orcam Financi…",urlToImage: "https://letstalkbitcoin.com/files/blogs/8676-35b88baed274e6a2a3d758cfbbfa2c247fe68e4b4c3350285e1f02d7155886f8.jpg"}])
const store = createStore(newsStore, []);

class ListPage extends Component {
    state = {
         listData:[],
Title:"",
Description:"",
ImageSource:"",
isLoading:true,
}

NewsSelected = (newsItem) =>{
    // this.setState({Title:newsItem.title});
    // this.setState({Description:newsItem.description});
    // this.setState({ImageSource:newsItem.urlToImage})
    this.props.navigation.navigate('NewsDetails',{  
        title: newsItem.title,  
        description: newsItem.description, 
        imageurl: newsItem.urlToImage == null ? "https://static2.seekingalpha.com/images/marketing_images/fair_use_logos_products/sacl_bitcoin_bitcoin.jpeg":newsItem.urlToImage  ,
        url:newsItem.url,
        source:newsItem.source.name,
        published:newsItem.publishedAt
    })
console.log(newsItem)
}

addNews = () =>{   store.dispatch({
    type: 'NEWS_ADD',
    news: {Id: this.state.listData.length+1, 
        title: "Bitcoin: A Value Investor's Take On This Asset Bubble","description":"Bitcoin's latest speculative boom is not one I will be participating in. There are too many risks and known unknowns for me to get comfortable with the bitcoin",
    description: "A internet nos permite enviar mensagens, compartilhar fotos, baixar músicas, assistir a vídeos... Mas cada uma destas atividades vem acompanhada de um pequeno custo para o meio ambiente.",
    urlToImage: "https://static2.seekingalpha.com/images/marketing_images/fair_use_logos_products/sacl_bitcoin_bitcoin.jpeg",
    source:{"id":null,"name":"Seeking Alpha"},
    publishedAt:"2020-06-07T16:31:27Z",
    url:"https://seekingalpha.com/article/4352394-bitcoin-value-investors-take-on-this-asset-bubble"}
  });}

fetchNewsData = () =>{

    fetch('http://newsapi.org/v2/everything?q=bitcoin&from=2020-07-25&sortBy=publishedAt&apiKey=3c2300055a4b413eb90eeff294e2eda8')
      .then((response) => response.json())
      .then((json) => {
        console.log("test web APi")  
        console.log(json.articles)
        store.dispatch({
        type: 'NEWS_FROM_WEB',newsList:json.articles
    
    })})
      .catch((error) => console.error(error))
      .finally(() => this.setState({isLoading:false}));
}


    render() { 
        this.props.navigation.setOptions({
            headerRight: () => (
              <Button onPress={() => this.addNews()} title="Add News" />
            ),
          });

        store.subscribe(() => this.setState({listData:store.getState()}))
        if (this.state.isLoading){
            this.fetchNewsData();
        }

        
        return (
              <View>
                   {this.state.isLoading ? <ActivityIndicator style = {{justifyContent:"center",alignItems:"center"}}/> : (
                  <FlatList
keyExtractor = {item => item.urlToImage}  
data={this.state.listData}
renderItem={({item}) => 
<TouchableWithoutFeedback onPress={ () => this.NewsSelected(item)}>
<View>
<View style = {{flexDirection:"row"}}>
    <Image style ={{width:100, marginLeft:10,marginRight:5,resizeMode: 'contain',alignItems:"stretch",justifyContent:"flex-start"}}  source = {item.urlToImage == null? {uri:"https://static2.seekingalpha.com/images/marketing_images/fair_use_logos_products/sacl_bitcoin_bitcoin.jpeg"}:{uri:item.urlToImage}}>
        
    </Image>
    <View style = {{flexDirection:"column",marginLeft:5,marginRight:10, marginTop:20, width: 0,
        flexGrow: 1,
        flex: 1,}}>
    <Text style = {{fontSize:16,fontWeight:"bold"}} >{item.title}</Text>
    <Text style = {{fontSize:10,color:'gray'}} >{item.description}</Text>

    </View>
    
    </View>
    <View style = {{backgroundColor:"black",marginTop:4,height:2}}/>
    </View>
    </TouchableWithoutFeedback>}
   
    />)}


        </View>);
    }
}
 
export default ListPage;