import React,{Component} from 'react';
import{View,Text,Image,Linking} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

class DetailsPage extends Component {
    state = {  }
    render() { 
        console.log("Details")
        console.log(this.props.route.params)
        console.log(this.props.route.params.url)
const imageSource = {uri:this.props.route.params.imageurl};
        return (
        <ScrollView style ={{borderColor:"gray", borderBottomWidth:2,borderTopWidth:2,borderLeftWidth:2,borderRightWidth:2,paddingLeft:5,paddingRight:5,paddingBottom:5,marginRight:0,marginLeft:0,marginTop:0 }}>
        <View  > 
             <View style = {{alignItems:"center"}}>
                 <Image style ={{width:150, height:150, marginLeft:10, marginTop:10,marginRight:5,resizeMode: 'cover',alignItems:"center",justifyContent:"center"}} source = {imageSource}/>                
                 
          </View>
            <View style ={{paddingLeft:5,paddingRight:5,paddingBottom:5,marginRight:2,marginLeft:2,marginTop:10 }}>
          <Text style = {{alignItems:"center",fontSize:22,marginTop:0,fontWeight:"bold",textAlign:"center"}}> {this.props.route.params.title}</Text>
        <Text style = {{alignItems:"center",fontSize:15,marginTop:15}}> {this.props.route.params.description}</Text>
          <View style = {{flexDirection:"row",marginTop:15,}} >
              <Text style ={{flexDirection:"row",fontWeight:"bold",fontSize:18,}} > Source : </Text>
              <Text style = {{fontSize:15,textDecorationLine: 'underline', marginTop:2}}> {this.props.route.params.source}</Text>
          </View>
          <View  style = {{flexDirection:"row",marginTop:15}}>
              <Text style ={{flexDirection:"row",fontWeight:"bold",fontSize:18,}} > Published at : </Text>
              <Text style = {{alignItems:"center",fontSize:15,textDecorationLine: 'underline',marginTop:2}}> {this.props.route.params.published}</Text>
          </View>
          <Text style = {{alignItems:"flex-start",fontSize:18,marginTop:15,fontWeight:"bold"}}> For More Info Visit : </Text>
                 <Text style = {{alignItems:"flex-start",fontSize:15,marginTop:5,color:'blue',textDecorationLine: 'underline'} }  onPress={() => Linking.openURL(this.props.route.params.url)}> {this.props.route.params.url}</Text>
          </View> 
          </View>
          </ScrollView>);
    }
}
 
export default DetailsPage;